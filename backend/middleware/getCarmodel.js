const Carmodel = require("../models/carmodel");

module.exports = async function (req, res, next) {
    let carmodel;
    try {
        carmodel = await Carmodel.findById(req.params.id);
        if (carmodel === null) {
            return res.status(404).json({message: "Cannot find carmodel."});
        }
    } catch (e) {
        return res.status(500).json({message: e.message});
    }
    res.carmodel = carmodel;
    next();
}
