const mongoose = require("mongoose");

const carmodelSchema = new mongoose.Schema({
     _id: {
         type: Number,
         required: true
     },
    brand: {
        type: String,
        required: true,
    },
    model: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    }
});

module.exports = mongoose.model("Carmodel", carmodelSchema);