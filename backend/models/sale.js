const mongoose = require("mongoose");

const saleSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    employee_id: {
        type: Number,
        required: true,
    },
    carmodel_id: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model("Sale", saleSchema);