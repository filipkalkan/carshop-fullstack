const mongoose = require("mongoose");
const Sale = require("./sale");

const employeeSchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true,
    },
    sales: {
        type: Number,
        required: false,
        default: 0
    },
    soldCars: {
        type: [Sale.schema],
        required: false,
        //default: undefined
    }
});

module.exports = mongoose.model("Employee", employeeSchema);