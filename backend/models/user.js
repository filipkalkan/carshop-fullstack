const jwt = require("jsonwebtoken");
const Joi = require("joi");
const mongoose = require("mongoose");
require("dotenv").config();

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 50
    },
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 1024
    },
    employeeNumber: {
        type: Number,
        required: false,
        maxlength: 1024
    },
    isAdmin: {
        type: Boolean,
        required: false
    }
});

userSchema.methods.generateAuthToken = function() {
    const token = jwt.sign(
        {
            _id: this._id,
            name: this.name,
            email: this.email,
            employeeNumber: this.employeeNumber,
            isAdmin: this.isAdmin
        },
        process.env.JWT_PRIVATE_KEY
    );
    return token;
};

function validateUser(user) {
    const schema = {
        name: Joi.string()
            .min(2)
            .max(50)
            .required(),
        email: Joi.string()
            .min(5)
            .max(255)
            .required()
            .email(),
        password: Joi.string()
            .min(5)
            .max(255)
            .required(),
        employeeNumber: Joi.number()
    };

    return Joi.validate(user, schema);
}

function validatePatch(body) {
    const schema = {
        name: Joi.string().max(255).allow(""),
        email: Joi.string().min(5).max(255).email().allow(""),
        password: Joi.string().min(5).max(255).allow(""),
        employeeNumber: Joi.number().min(1).max(255).allow(""),
        isAdmin: Joi.boolean()
    };

    return Joi.validate(body, schema);
}

exports.User = mongoose.model("User", userSchema);
exports.validate = validateUser;
exports.validatePatch = validatePatch;
