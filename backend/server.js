const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();

require("dotenv").config();

const port = process.env.PORT || 3900;


mongoose.connect(process.env.DATABASE_URL, {useNewUrlParser: true});
const db = mongoose.connection;
db.on("error", (error) => console.error(error));
db.once("open", () => console.log("connected to database"));

app.use(express.json());
app.use(cors());

const modelsRouter = require("./routes/carmodels");
const employeesRouter = require("./routes/employees");
const totalSalesRouter = require("./routes/total_sales");
const usersRouter = require("./routes/users");
const authorizationRouter = require("./routes/authorization");

app.use("/api/carmodels", modelsRouter);
app.use("/api/employees", employeesRouter);
app.use("/api/total_sales", totalSalesRouter);
app.use("/api/users", usersRouter);
app.use("/api/authorization", authorizationRouter);


app.listen(port, () => console.log(`Server listening on port ${port}...`));