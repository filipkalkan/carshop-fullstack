const Carmodel = require("./models/carmodel");
const Employee = require("./models/employee");
const Sale = require("./models/sale");
const mongoose = require("mongoose");
const fs = require("fs");
require("dotenv").config();

async function populateEmployeesInDb(employees) {
    for (let employee of employees) {
        const newEmployee = new Employee({
            _id: employee.id,
            name: employee.name
        });
        await newEmployee.save();
    }
}

async function populateCarmodelsInDb(carmodels) {
    for (let carmodel of carmodels) {
        const newCarmodel = new Carmodel({
            _id: carmodel.id,
            brand: carmodel.brand,
            model: carmodel.model,
            price: carmodel.price
        });
        await newCarmodel.save();
    }
}

async function populateSalesInDb(sales) {
    for (let sale of sales) {
        const newSale = new Sale({
            _id: sale.id,
            employee_id: sale.employee_id,
            carmodel_id: sale.carmodel_id
        });
        await newSale.save();
    }
}

function parseData() {
    const jsonData = fs.readFileSync("data.json");
    const data = JSON.parse(jsonData);
    return data;
}

async function seed() {
    await mongoose.connect(process.env.DATABASE_URL, {useNewUrlParser: true});

    await Carmodel.deleteMany({});
    await Employee.deleteMany({});
    await Sale.deleteMany({});

    const data = parseData();

    const {employees, carmodels, sales} = data.carshop;

    await populateEmployeesInDb(employees);
    console.log("Populated employees...");
    await populateCarmodelsInDb(carmodels);
    console.log("Populated carmodels...");
    await populateSalesInDb(sales);
    console.log("Populated sales...");

    mongoose.disconnect();

    console.log("Done!");
}

seed();
