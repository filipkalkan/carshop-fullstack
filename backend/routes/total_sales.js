const express = require("express");
const router = express.Router();
const Sale = require("../models/sale");
const Employee = require("../models/employee");
const Carmodel = require("../models/carmodel");

router.get("/", async (req, res) => {
    try{
        const totalSales = await getTotalSales();
        res.json(totalSales);
    } catch (e) {
        res.status(500).json({message: e.message});
    }
});

async function updateEmployee(referencedEmployee, referencedCarmodel, salesReference) {
    referencedEmployee.sales += referencedCarmodel.price;
    referencedEmployee.soldCars.push(salesReference);
    await referencedEmployee.save();
}

async function updateEmployees(salesReferences, employees) {
    for (let salesReference of salesReferences) {
        const referencedEmployee = employees.filter(employee => employee._id === salesReference.employee_id)[0];
        const referencedCarmodel = await Carmodel.findById(salesReference.carmodel_id);
        await updateEmployee(referencedEmployee, referencedCarmodel, salesReference);
    }
}

async function getTotalSales() {
    const salesReferences = await Sale.find();
    let employees = await Employee.find();
    employees = employees.map(employee => {
        employee.sales = 0;
        employee.soldCars = employee.soldCars.filter(soldCar => false); //Not sure if it is okay to set =[] due to schema for employee
        return employee;
    });

    await updateEmployees(salesReferences, employees);

    return employees;
}

module.exports = router;