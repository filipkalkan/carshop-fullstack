const express = require("express");
const router = express.Router();
const Carmodel = require("../models/carmodel");
const getCarmodel = require("../middleware/getCarmodel");

router.get("/", async (req, res) => {
    try{
        const carmodels = await Carmodel.find();
        res.json(carmodels);
    } catch (e) {
        res.status(500).json({message: e.message});
    }
});

router.post("/", async (req, res) => {
    const reverseOrderedCarmodels = await Carmodel.find().sort({"_id": -1});
    greatestId = reverseOrderedCarmodels[0]._id;
    const carmodel = new Carmodel({
        _id: (req.body._id || greatestId + 1),
        brand: req.body.brand,
        model: req.body.model,
        price: req.body.price
    });

    try{
        const newCarmodel = await carmodel.save();
        res.status(201).json(newCarmodel);
    } catch (e) {
        res.status(400).json({message: e.message});
    }

});

router.delete("/:id", async (req, res) => {
    try{
        const deletedCarmodel = await Carmodel.findByIdAndRemove(req.params.id);
        if(!deletedCarmodel){
            return res.status(404).send("The car model with the given id was not found.");
        }
        res.json(deletedCarmodel);
    } catch (e){
        res.status(500).json({message: e.message});
    }
})

module.exports = router;