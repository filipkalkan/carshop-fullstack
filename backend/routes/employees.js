const express = require("express");
const router = express.Router();
const Employee = require("../models/employee");

router.get("/", async (req, res) => {
    try{
        let employees = await Employee.find().select("-sales");
        res.json(employees);
    } catch (e) {
        res.status(500).json({message: e.message});
    }
});

module.exports = router;