const authorize = require("../middleware/authorize");
const bcrypt = require("bcryptjs");
const {User, validate, validatePatch} = require("../models/user");
const express = require("express");
const router = express.Router();

router.get("/me", authorize, async (req, res) => {
    const user = await User.findById(req.user._id).select("-password"); //Exclude the password
    res.send(user);
});

router.get("/", authorize, async (req, res) => {
    const users = await User.find().select("-password"); //Exclude the password
    res.send(users);
});

router.patch("/:_id", async (req, res) => {
    const { error } = validatePatch(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    const userUpdates = createUserUpdates(req);

    if(userUpdates.password){
        userUpdates.password = bcrypt.hashSync(userUpdates.password, 8);
    }

    const newUser = await User.findByIdAndUpdate(
        req.params._id,
        userUpdates,
        { new: true }
    );


    if (!newUser)
        return res.status(404).send("The user with the given ID was not found.");

    res.send(newUser);
});

router.post("/", async (req, res) => {
    const {error} = validate(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    let user = await User.findOne({email: req.body.email});
    if (user) {
        return res.status(400).send("User already registered.");
    }

    user = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        employeeNumber: req.body.employeeNumber,
        isAdmin: req.body.isAdmin
    });
    user.password = bcrypt.hashSync(user.password, 8);
    await user.save();

    const token = user.generateAuthToken();
    res
        .header("x-auth-token", token)
        .header("access-control-expose-headers", "x-auth-token")
        .send({
            _id: user._id,
            name: user.name,
            email: user.email,
            employeeNumber: user.employeeNumber
        });
});

router.delete("/:id", async (req, res) => {
    try{
        const deletedUser = await User.findByIdAndRemove(req.params.id);
        if(!deletedUser){
            return res.status(404).send("The user model with the given id was not found.");
        }
        res.json(deletedUser);
    } catch (e){
        res.status(500).json({message: e.message});
    }
})

function createUserUpdates(req) {
    const userUpdates = {};
    for (let property of Object.keys(req.body)) {
        if (property && req.body[property] !== "") {
            console.log("Changing " + property + " to " + req.body[property])
            userUpdates[property] = req.body[property];
        }
    }
    return userUpdates;
}

module.exports = router;
