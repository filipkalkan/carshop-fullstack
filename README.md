<h1>Car Shop</h1>
A React web application with MongoDB backend.

<h2>Running the app</h2>
<h3>Requirements</h3>
Node.js and concurrently must be installed.
<h3>Installing concurrently</h3>
Run the following command in the terminal:
<code>npm i concurrently</code>
<h3>Start</h3>
Start by executing <code>npm start</code> in the terminal while located in carshop-fullstack directory.