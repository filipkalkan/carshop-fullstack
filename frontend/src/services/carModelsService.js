import http from "./httpService";

const apiEndPoint = process.env.REACT_APP_API_URL + "/carmodels"; //URL should be extracted to env variable

export async function getCarModels(){
        return await http.get( apiEndPoint);
}

export async function saveCarModel(carModel){
        if(carModel._id) {
                const promise = await http.delete(apiEndPoint + `/${carModel._id}`);    //Could implement and use put instead.
        }
        return await http.post(apiEndPoint, carModel);
}

export async function deleteCarModel(_id){
        return await http.delete(apiEndPoint + `/${_id}`);
}

export async function getCarModel(_id){
        const {data: allCarModels} = await http.get( apiEndPoint);
        const carModel = allCarModels.find(carModel => carModel._id == _id);
        if(!carModel){
                throw new Error({message: "Car model not found."});
        }
        return carModel;
}

export default {
        getCarModels,
        saveCarModel,
        getCarModel,
        deleteCarModel
}