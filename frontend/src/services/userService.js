import http from "./httpService";

const apiEndPoint = process.env.REACT_APP_API_URL + "/users";

export async function registerUser(user){
    const body = {
        email: user.username,
        password: user.password,
        name: user.name,
        employeeNumber: user.employeeNumber
    }
    return await http.post(apiEndPoint, body);
}

export async function getAllUsers() {
    return await http.get(apiEndPoint);
}

export async function patchUser(user) {
    const _id = user._id;
    delete user._id;
    return await http.patch(apiEndPoint + "/" + _id, user);
}

export async function deleteUser(_id) {
    return await http.delete(apiEndPoint + "/" + _id);
}

export default {
    registerUser,
    getAllUsers,
    patchUser,
    deleteUser
}