import http from "./httpService";
import jwtDecode from "jwt-decode";

const tokenKey = "token";
const apiEndPoint = process.env.REACT_APP_API_URL + "/authorization";

http.setJwt(getJwt());

export async function login(email, password){
    const body = {
        email,
        password
    }
    const {data:jwt} = await http.post(apiEndPoint, body);
    localStorage.setItem(tokenKey, jwt);
}

export function loginWithJwt(jwt){
    localStorage.setItem(tokenKey, jwt);
}

export function logout() {
    localStorage.removeItem(tokenKey);
}

export function getCurrentUser() {
    let user;
    try{
        const jwt = localStorage.getItem(tokenKey);
        user = jwtDecode(jwt);
    } catch (e) {
        user = null;
    }
    return user;
}

export function getJwt() {
    return localStorage.getItem(tokenKey);
}

export function isAdmin() {
    if(getCurrentUser()){
        const jwt = localStorage.getItem(tokenKey);
        const decoded = jwtDecode(jwt);
        return decoded.isAdmin;
    } else{
        return false;
    }

}

export default {
    login,
    loginWithJwt,
    logout,
    getCurrentUser,
    getJwt,
    isAdmin
}