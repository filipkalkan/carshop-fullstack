import http from "./httpService";

const apiEndPoint = process.env.REACT_APP_API_URL + "/total_sales";

export default async function getAllSales(){
    return await http.get(apiEndPoint);
}