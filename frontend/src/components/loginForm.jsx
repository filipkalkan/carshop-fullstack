import React, {Component} from 'react';
import Joi from "joi-browser";
import Form from "../common/form";
import * as auth from "../services/authorizationService"
import {Redirect} from "react-router-dom";

class LoginForm extends Form {
    constructor(props) {
        super(props);
        this.state = {
            data: {username: "", password: ""},
            errors: {}
        }
    }

    schema = {
        username: Joi.string().required().label("Username"),
        password: Joi.string().required().label("Password")
    }

    async doSubmit(){
        const {username, password} = this.state.data;
        try{
            await auth.login(username, password);
            const {state} = this.props.location;
            window.location = state ? state.from.pathname : "/"; //Causes full reload of page. So the App component mounts again, updating navbar. If state is falsy "from" has no value.
        } catch(e){
            if(e.response && e.response.status === 400){
                const errors = {...this.state.errors};
                errors.username = e.response.data;
                this.setState({errors});
            }
        }
    }

    render() {
        if(auth.getCurrentUser()) return <Redirect to="/"/>;
        return (
            <div>
                <h1>Login</h1>
                <form onSubmit={this.handleSubmit}>
                    {this.renderInput("username", "Username")}
                    {this.renderInput("password", "Password", "password")}
                    {this.renderButton("login")}
                </form>
            </div>
        );
    }
}

export default LoginForm;