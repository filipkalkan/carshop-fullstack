import React, {Component} from 'react';
import Form from "../common/form";
import Joi from "joi-browser";
import * as userService from "../services/userService";
import {toast} from "react-toastify";


class EditUserForm extends Form {
    state = {
        data: {
            email: "",
            password: "",
            name: "",
            employeeNumber: "",
        },
        errors: {}
    };

    schema = {
        email: Joi.string()
            .label("UserName").allow(""),
        password: Joi.string().allow("")
            .label("Password"),
        name: Joi.string().allow("")
            .label("Name"),
        employeeNumber: Joi.string()
            .label("Employee Number").allow(""),
    };

    async populateUser() {
        try {
            const userId = this.props.match.params.id;
            const {data: allUsers} = await userService.getAllUsers();
            const user = allUsers.find(user => user._id == userId);
            console.log(user);
            this.setState({ data: {
                    email: user.email,
                    password: "",
                    name: user.name,
                    employeeNumber: user.employeeNumber.toString() || "",
                } });
        } catch (ex) {
            if (ex.response && ex.response.status === 404 && this.props.match.params.id)
                this.props.history.replace("/not-found");
        }
    }

    async componentDidMount() {
        await this.populateUser();
    }

    doSubmit = async () => {
        const {params} = this.props.match;
        const user = {...this.state.data};
        let employeeNumber;
        if(user.employeeNumber !== "") {
            employeeNumber = parseInt(user.employeeNumber);
            if (isNaN(employeeNumber)) {
                const errors = {employeeNumber: "Employee Number must be a number."};
                this.setState({errors});
                return;
            }
        }
        user.employeeNumber = employeeNumber;
        user._id = params.id;
        try{
            await userService.patchUser(user);
            toast.info("Modified user sucessfully!");
        } catch (e){
            toast.info("Something went wrong while modifying user.");
        }
        this.props.history.push("/profile");
    };

    render() {
        return (
            <div>
                <h1>Edit User</h1>
                <form onSubmit={this.handleSubmit}>
                    {this.renderInput("email", "Email")}
                    {this.renderInput("password", "Password")}
                    {this.renderInput("name", "Name")}
                    {this.renderInput("employeeNumber", "Employee Number")}
                    {this.renderButton("Submit Changes")}
                </form>
            </div>
        );
    }
}

export default EditUserForm;