import React from 'react';
import car_icon from "../car_icon.png";
import DropDownMenu from "./dropDownMenu";

function CarModelCard({onEdit, onDelete, carModel, user}) {
    return (
        <div className="row">
            <div className="col-12 mt-3">
                <div className="card card-hover" style={{overflow: "hidden"}}>
                    <div className="card-horizontal">
                        <div className="img-square-wrapper">
                            <img className="car-model-image" src={car_icon} alt="Card image cap"/>
                        </div>
                        <div className="card-body">
                            {user && user.isAdmin && <DropDownMenu
                                onEdit={onEdit}
                                onDelete={onDelete}
                                carModel={carModel}
                            />}
                            <h4 className="card-title">{carModel.brand}</h4>
                            <h6 className="card-title">{carModel.price} SEK</h6>
                            <p className="card-text"> BMW 3 Series 335i with RWD, Technology Package, Premium Package,
                                M Sport Package, Driver Assistance Package, Luxury Line ...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CarModelCard;