import React from 'react';
import {NavLink, Link} from "react-router-dom";

const NavBar = ({user}) => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-csGreen">
            <div className="container">
                <Link className="navbar-brand" to="/">Car Shop</Link>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/models">Models</NavLink>
                        </li>
                        {!user &&
                        <React.Fragment>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/login">Login</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/register">Register</NavLink>
                            </li>
                        </React.Fragment>}
                        {user && user.employeeNumber &&
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/profile">Profile</NavLink>
                        </li>}
                        {user &&
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/logout">Logout</NavLink>
                        </li>
                        }
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default NavBar;