import React, {Component} from 'react';
import getAllSales from "../services/salesService";
import * as authorizationService from "../services/authorizationService";
import carModelsService, {getCarModel} from "../services/carModelsService";

class SalesTable extends Component {
    constructor() {
        super();
        this.state = {
            soldCarModels: [],
            employeeSales: {}
        }
    }

    async componentDidMount() {
        const {data: employeesWithSales} = await getAllSales();
        const employeeSales = employeesWithSales.find(employee => employee._id === authorizationService.getCurrentUser().employeeNumber); //REFACTOR?
        const soldCarModels = await this.getSoldCarModels(employeeSales);
        this.setState({soldCarModels, employeeSales});
    }

    async getSoldCarModels(employeeSales) {
        const soldCarsReferences = employeeSales.soldCars;
        const soldCarModelsPromises = await soldCarsReferences.map(async references => await getCarModel(references.carmodel_id));
        const soldCarModels = await Promise.all(soldCarModelsPromises);
        return soldCarModels;
    }

    render() {
        const {soldCarModels, employeeSales} = this.state;
        return (
            <div>
                <h3 className="my-4">Sales</h3>
                <table className="table table-hover my-3">
                    <thead>
                    <tr>
                        <th>Brand</th>
                        <th>Car Model</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    {soldCarModels.map(carModel => <tr>
                        <td>{carModel.brand}</td>
                        <td>{carModel.model}</td>
                        <td>{carModel.price} SEK</td>
                    </tr>)}
                    <tr>
                        <td></td>
                        <td></td>
                        <td><b>Total</b>: {employeeSales.sales} SEK</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default SalesTable;