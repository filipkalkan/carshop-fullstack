import React, {Component} from 'react';
import Form from "../common/form";
import Joi from "joi-browser";
import * as userService from "../services/userService";
import authorizationService from "../services/authorizationService";


class RegisterForm extends Form {
    schema = {
        username: Joi.string().email().required().label("Username"),
        password: Joi.string().min(5).required().label("Password"),
        name:   Joi.string().required().label("Name"),
        employeeNumber: Joi.string().label("Employee Number")
    }


    async doSubmit(){
        const {data} = this.state;
        try{
            const response = await userService.registerUser(data);
            await authorizationService.loginWithJwt(response.headers["x-auth-token"]);
            window.location = "/";
        } catch (e) {
            if(e.response && e.response.status === 400){
                const errors = {...this.state.errors};
                errors.username = "This user already exists.";
                this.setState({errors});
            }
        }
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    {this.renderInput("username", "Username")}
                    {this.renderInput("password", "Password", "password")}
                    {this.renderInput("name", "Name")}
                    {this.renderInput("employeeNumber", "Employee Number (Optional)")}
                    {this.renderButton("Register")}
                </form>
            </div>
        );
    }
}

export default RegisterForm;