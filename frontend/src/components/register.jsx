import React, {Component} from 'react';
import RegisterForm from "./registerForm";

class Register extends Component {
    render() {
        return (
            <div>
                <h1>Register</h1>
                <RegisterForm history={this.props.history}/>
            </div>
        );
    }
}

export default Register;