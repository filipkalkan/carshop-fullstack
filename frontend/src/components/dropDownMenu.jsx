import React from 'react';

function DropDownMenu({onEdit, onDelete, carModel}) {
    return (
        <div className="dropdown float-right">
            <i className="fa fa-cog" type="button"
               id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
               aria-expanded="false"/>
            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item clickable" onClick={() => onEdit(carModel._id)}>Edit</a>
                <a className="dropdown-item clickable" onClick={() => onDelete(carModel._id)}>Delete</a>
            </div>
        </div>
    );
}

export default DropDownMenu;