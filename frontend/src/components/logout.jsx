import React, {Component} from 'react';
import authorizationService from "../services/authorizationService";

class Logout extends Component {

    componentDidMount() {
        authorizationService.logout();
        window.location = "/";
    }

    render() {
        return null;
    }
}

export default Logout;