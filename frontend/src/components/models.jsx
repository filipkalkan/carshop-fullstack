import React, {Component} from 'react';
import carModelsService from "../services/carModelsService";
import CarModelList from "./carModelList";
import {toast} from "react-toastify";
import authorizationService from "../services/authorizationService";

class Models extends Component {
    constructor(props) {
        super(props);
        this.state = {
            carModels: [],
            user: authorizationService.getCurrentUser(),
        }
    }

    async componentDidMount() {
        const {data: carModels} = await carModelsService.getCarModels();
        this.setState({carModels});
    }

    async handleDelete(_id) {
        const oldCarModels = [...this.state.carModels];
        let carModels = [...this.state.carModels];
        carModels = carModels.filter(carModel => carModel._id !== _id);
        try{
            await carModelsService.deleteCarModel(_id);
            toast.info("Deleted car model successfully");
            this.setState({carModels});
        } catch (e) {
            toast.error("Someting went wrong while deleting car model.");
            this.setState({oldCarModels});
        }

    }

    handleEdit(_id) {
        this.props.history.push(`/edit/${_id}`);
    }

    render() {
        const {carModels, user} = this.state;
        return (
            <div>
                <div className="row justify-content-end">
                    {user && user.isAdmin &&
                    <button className="btn btn-secondary bg-csGreen mt-3 mr-3"
                          onClick={() => this.handleEdit("new")}
                    >Add Car Model</button>}
                </div>
                <CarModelList carModels={carModels}
                              onDelete={(_id) => this.handleDelete(_id)}
                              onEdit={(_id) => this.handleEdit(_id)}
                />
            </div>
        );
    }
}

export default Models;