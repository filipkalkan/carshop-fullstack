import React, {Component} from 'react';
import authorizationService from "../services/authorizationService";
import getAllSales from "../services/salesService";
import userService from "../services/userService";
import {toast} from "react-toastify";

class AccountsTool extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allUsers: [],
            employeesWithSales: [],
        }
    }

    async componentDidMount() {
        const {data: employeesWithSales} = await getAllSales();
        const {data: allUsers} = await userService.getAllUsers();
        this.setState({employeesWithSales, allUsers});
    }

    editUser(_id){
        this.props.history.push(`/editUser/${_id}`);
    }

    async deleteUser(_id){
        const oldUsers = [...this.state.allUsers];
        let users = [...this.state.allUsers];
        users = users.filter(user => user._id !== _id);
        this.setState({allUsers: users});

        try{
            await userService.deleteUser(_id);
            toast.info("User sucessfully deleted!");
        } catch (e) {
            this.setState({allUsers: oldUsers});
            toast.error("Something went wrong while deleting user.");
        }
    }

    render() {
        const {allUsers, employeesWithSales} = this.state;
        return (
            <div>
                <h3 className="my-4">Users</h3>
                <table className="table table-hover my-3">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Employee Number</th>
                        <th>Total Sales</th>
                        <th>Admin</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {allUsers.map(user =>{
                        return (
                            <tr key={user._id}>
                                <td>{user.name}</td>
                                <td>{user.email}</td>
                                <td>{user.employeeNumber || "-"}</td>
                                <td>{user.employeeNumber ?
                                    employeesWithSales.find(employeeWithSales => employeeWithSales._id === user.employeeNumber).sales + " SEK" :
                                    "-"}
                                </td>
                                <td>{user.isAdmin ? "Yes" : "No"}</td>
                                <td><button className="btn btn-secondary" onClick={() => this.editUser(user._id)}>Edit</button></td>
                                <td><button className="btn btn-danger" onClick={() => this.deleteUser(user._id)}>Delete</button></td>
                            </tr>
                    )})}
                    </tbody>
                </table>

            </div>
        );
    }
}

export default AccountsTool;