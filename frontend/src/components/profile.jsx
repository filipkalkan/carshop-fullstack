import React, {Component} from 'react';
import SalesTable from "./salesTable";
import authorizationService from "../services/authorizationService";
import AccountsTool from "./accountsTool";

class Profile extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const user = authorizationService.getCurrentUser();
        return (
            <div>
                <h1 className="my-3">{user.name}</h1>
                {user.employeeNumber && <span className="my-3">Employee number: {user.employeeNumber}</span>}
                {user.employeeNumber && <SalesTable {...this.props}/>}
                {user.isAdmin && <AccountsTool {...this.props} />}
            </div>
        );
    }
}

export default Profile;