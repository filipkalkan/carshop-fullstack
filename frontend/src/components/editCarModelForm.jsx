import React from "react";
import Joi from "joi-browser";
import Form from "../common/form";
import {getCarModel, saveCarModel} from "../services/carModelsService";
import {toast} from "react-toastify";

class EditCarModelForm extends Form {
    state = {
        data: {
            brand: "",
            model: "",
            price: 0
        },
        errors: {}
    };

    schema = {
        brand: Joi.string()
            .required()
            .label("Brand"),
        model: Joi.string()
            .required()
            .label("Model"),
        price: Joi.string()
            .required()
            .label("Price"),
    };

    async populateCarModel() {
        try {
            const carModelId = this.props.match.params.id;
            if (carModelId === "new") {
                return;
            }
            const carModel = await getCarModel(carModelId);
            this.setState({ data: {
                    brand: carModel.brand,
                    model: carModel.model,
                    price: carModel.price.toString()
                } });
        } catch (ex) {
            if (ex.response && ex.response.status === 404 && this.props.match.params.id)
                this.props.history.replace("/not-found");
        }
    }

    async componentDidMount() {
        await this.populateCarModel();
    }

    doSubmit = async () => {
        const {params} = this.props.match;
        const carModel = {...this.state.data};
        const price = parseInt(carModel.price);
        if(isNaN(price)){
            const errors = {price: "Price must be a number."};
            this.setState({errors});
            return;
        }
        carModel.price = price;
        if(params.id !== "new"){
            carModel._id = params.id;
        }
        try{
            await saveCarModel(carModel);
            toast.info("Added car model sucessfully!");
        } catch (e) {
            toast.error("Something went wrong while adding car model.")
        }
        this.props.history.push("/models");
    };

    render() {
        return (
            <div>
                <h1>Edit Car Model</h1>
                <form onSubmit={this.handleSubmit}>
                    {this.renderInput("brand", "Brand")}
                    {this.renderInput("model", "Model")}
                    {this.renderInput("price", "Price")}
                    {this.renderButton("Save")}
                </form>
            </div>
        );
    }
}

export default EditCarModelForm;
