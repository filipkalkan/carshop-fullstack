import React from 'react';
import authorizationService from "../services/authorizationService";
import CarModelCard from "./carModelCard";

function CarModelList(props) {
    const user = authorizationService.getCurrentUser();
    const {carModels} = props;
    return (
        <div>
            <ul className="list-unstyled my-3">
                {carModels.map(model =>
                    <CarModelCard carModel={model} user={user} {...props}/>)}
            </ul>
        </div>
    );
}

export default CarModelList;