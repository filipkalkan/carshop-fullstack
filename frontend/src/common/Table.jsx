import React from 'react';
import TableHeader from "./TableHeader";
import TableBody from "./TableBody";

const Table = ({sortColumn, onSort, columns, data}) => {
    return (
        <table className='table'>
            <TableHeader
                sortColumn={sortColumn}
                onSort={(sortColumn) => onSort(sortColumn)}
                columns={columns}
            />
            <TableBody
                data={data}
                columns={columns}
            />
        </table>
    );
};

export default Table;