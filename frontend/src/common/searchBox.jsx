import React from 'react';

function SearchBox(props) {
    const {onChange, value} = props;
    return (
        <form>
            <input
                className="form-control my-3"
                placeholder="Search..."
                onChange={e => onChange(e.currentTarget.value)}
                value={value}
            />
        </form>
    );
}

export default SearchBox;