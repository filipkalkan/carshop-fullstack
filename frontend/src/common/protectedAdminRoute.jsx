import React from 'react';
import {Redirect, Route} from "react-router-dom";
import authorizationService from "../services/authorizationService";

function ProtectedAdminRoute({user, render, component: Component, ...rest}) {
    return (
        <Route
            {...rest}
            render={props => {
                if (!authorizationService.getCurrentUser().isAdmin) return <Redirect to={{
                    pathname: "/",
                    state: {from: props.location}
                }}
                />
                else {
                    return (Component ? <Component {...props} /> : render(props));
                }
            }}
        />
    );

}

export default ProtectedAdminRoute;