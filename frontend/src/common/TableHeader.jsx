import React, {Component} from 'react';

class TableHeader extends Component {
    raiseSort(path){
        let sortColumn = {...this.props.sortColumn};
        if(sortColumn.path === path){
            sortColumn.order = (sortColumn.order === "asc" ? "des" : "asc");
        } else{
            sortColumn = {path: path, order: "asc"};
        }
        this.props.onSort(sortColumn);
    }

    renderSortIcon(column){
        if(this.props.sortColumn.path !== column.path){
            return null;
        }
        return this.props.sortColumn.order == "asc" ? <i className="fa fa-sort-asc"></i> : <i className="fa fa-sort-desc"></i>
    }

    render() {
        return (
            <thead>
            <tr>
                {this.props.columns.map(c =>
                    <th key={(c.path || c.key)} className="clickable" onClick={() => this.raiseSort(c.path)}>{c.label} {this.renderSortIcon(c)}</th>
                )}
            </tr>
            </thead>
        );
    }
}

export default TableHeader;