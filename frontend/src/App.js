import './App.css';
import "react-toastify/dist/ReactToastify.css"
import React, {Component} from 'react';
import {ToastContainer} from "react-toastify";
import {Switch, Route, Redirect} from "react-router-dom";
import NavBar from "./components/navBar";
import LoginForm from "./components/loginForm";
import Logout from "./components/logout";
import NotFound from "./components/notFound";
import Register from "./components/register";
import Models from "./components/models";
import * as auth from "./services/authorizationService"
import ProtectedRoute from "./common/protectedRoute";
import EditCarModelForm from "./components/editCarModelForm";
import Profile from "./components/profile";
import ProtectedAdminRoute from "./common/protectedAdminRoute";
import EditUserForm from "./components/editUserForm";

class App extends Component{
    state = {
        user: null
    }

    componentDidMount() {
        const user = auth.getCurrentUser();
        this.setState({user});
    }

  render(){
    return(
        <div className="page-container">
        <main className="container-fluid px-0" style={{paddingBottom: "10vh"}}>
          <ToastContainer/>
          <NavBar user={this.state.user}/>
          <div className="container">
            <Switch>
              <Route path="/logout" component={Logout}/>
              <Route path="/login" component={LoginForm}/>
              <Route path="/models" component={Models}/>
              <Route path="/register" component={Register}/>
              <ProtectedRoute path="/profile" component={Profile}/>
              <ProtectedAdminRoute path="/editUser/:id" component={EditUserForm}/>
              <ProtectedRoute path="/edit/:id" component={EditCarModelForm}/>
              <Route path="/not-found" component={NotFound}/>
              <Redirect from="/" to="/models" exact/>
              <Redirect to="/not-found"/>
            </Switch>
          </div>
        </main>
            <footer className="footer bg-csGreen">
                <div className="container">
                </div>
            </footer>
        </div>
    );
  }
}

export default App;
